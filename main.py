import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_log_error
import math
data = pd.read_csv('house_pricing.csv')
data.drop('Unnamed: 0', axis=1, inplace=True)

#перетворення categorial data в features
data["Utilities"] = data["Utilities"].astype('category').cat.codes
#data['Street'] = data['Street'].astype('category').cat.codes
data['PoolQC'] = data['PoolQC'].astype('category').cat.codes
#data['FireplaceQu'] = data['FireplaceQu'].astype('category').cat.codes
#X_columns = ['Utilities', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['Street', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['PoolQC', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['FireplaceQu', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']


#X_columns = ['LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']


data = pd.get_dummies(data, columns=['Street','FireplaceQu'])
#X_columns = ['FireplaceQu_Gd', 'FireplaceQu_Fa', 'FireplaceQu_Ex','LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['PoolQC_Gd', 'PoolQC_Fa', 'PoolQC_Ex', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['Street_Pave', 'Street_Grvl', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
#X_columns = ['Utilities_NoSeWa', 'Utilities_AllPub', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']
X_columns = ['PoolQC', 'Utilities', 'FireplaceQu_Gd', 'FireplaceQu_Fa', 'FireplaceQu_Ex', 'Street_Pave', 'Street_Grvl', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt','1stFlrSF', '2ndFlrSF', 'GrLivArea', 'GarageArea', 'GarageCars','PoolArea']



Y_column = ['SalePrice']
X_data = data[X_columns]
Y_data = data[Y_column]
reg = LinearRegression().fit(X_data, Y_data)
Y_hat = reg.predict(X_data)
data['SalePrice_prediction'] = Y_hat
data['price_delta'] = data['SalePrice'] - data['SalePrice_prediction']
data[['SalePrice_prediction', 'SalePrice', 'price_delta']]
Sum1 = 0
Sum2 = 0
Sum3 = 0
for i in range(data.shape[0]):
    Sum1 += math.fabs(data['price_delta'][i])
    Sum2 += pow(data['price_delta'][i], 2)
    Sum3 += math.fabs(data['price_delta'][i]) / data['SalePrice'][i]
#print("MAE =",Sum1/data.shape[0])
#print("MSE =",Sum2/data.shape[0])
print("MAPE =", Sum3/data.shape[0])

#obj_df["body_style"] = obj_df["body_style"].astype('category')
#obj_df["body_style_cat"] = obj_df["body_style"].cat.codes